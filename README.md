# Vizir "show me the code" challenge solution

### Before start

The challenge is not complete because there are some validations remaining. I wrote a list of TODOs for both the backend and the front-end

### Description

To solve the "show me the code" challenge and make use of the evaluation criterias propose.

I developed the following two projects:

    - rails_version -> `API using rails`
    - react_front_end -> `front-end using React`

I guided my project through the following steps:

    - Define business objectives
    
        Problem: customers need to be able to calculate the price of a phone call and the company need a web page to provide transparency to their clients.
        
        Impact: as a result of building a web application decoupled in back-end and front-end, developers can extend its features easily and integrate with other services; and customers will have a dedicated web page.

    - Outline key user stories

        As a client, I want to calculate the price of a phone call with details like the price with and without a plan

        As a developer, I want to be able to integrate the back-end with different front-end applications (agnostic back-end)

        As a developer, I want to be able to use concepts and principles like REST (Representational State Transfer), DRY (Don’t Repeat Yourself), CRUD (Create, Read, Update, Delete), KISAP (Keep It Simple as Possible), and COC (Convention over Configuration) 

        As a developer, I want to use agile development 

    - Select technology architecture 

        Aftwer a quick research, I planned to develop an application composed by an API and a web front-end. It emulates the communication process between these two parts, like a real-world software. 

        Paradigm: REST
        Reason: it is resource-oriented, a standard method(name, arguments, status codes) ,utilizes HTTP features and easy to maintain

    - Write API and server specification

#### `rails_api`

By default, server will run on port `3333`  and can be accessible by going to **http://localhost:3333**

#### `react_front_end`

Developed a web front-end using React and JavaScript

By default, the front-end will run on port `3000` and can be accessible by going to **http://localhost:3000**