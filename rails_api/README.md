# Telzir Calculator API 

### Why rails and ruby? 

 - Mature framework and language
 - Easy to prototype and a lot of tools out of the box

### TODO list

 - Handle exceptions in the models
 - Implement integration tests and more some unit tests
 - Research about HTTP requests; should I use a get or post when data is sent to the backend but no resource is created?

### Installation and usage

1. Install Ruby

  - https://www.ruby-lang.org/en/documentation/installation/

2. Install Bundler

  - `gem install bundler`

3. Configure postgres if is not installed and the credentials in .env file

  - [Tutorial here](https://www.postgresqltutorial.com/install-postgresql/)

4. Install the project dependencies

  - `bundle install`

5. Running the project

  - `rails server -p 3333` - will run on **http://localhost:3333**

After these steps, accessing the server and requesting the routes described in the end of this file will return the data in json format.

### Running Specs

The project tests were developed using [RSpec](https://rspec.info/) is a test framework. To run the tests, just type:

  - `bundle exec rspec`

### Lint

The project lint uses the configuration provided by [RuboCop](https://rubocop.readthedocs.io/en/stable/) gem. To run the linter, just type:

  - `rubocop`

### Routes

The following routes were developed in the project:

  - `GET /api/v1/fees` - returns a json list of the available codes and plans
  - `GET /api/v1/fees/calculate?origin=<origin>&destination=<destination>&time=<time>&plan_id=<plan_id>` - returns a json with the calculations from selected input
