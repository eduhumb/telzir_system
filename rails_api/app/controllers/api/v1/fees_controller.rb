module Api
  module V1
    class FeesController < ApplicationController

      before_action :set_code_list, :set_phone_plan_list, only: [:index]

      # GET /api/v1/fees
      def index
        render json: {codes: @code_list, plans: @phone_plan_list}
      end

      # GET /api/v1/fees/calculate
      def show
        result = make_calculation
        render json: result
      end

      private

      def set_code_list
        @code_list = Fee.select(:id, :origin, :destination)
      end

      def set_phone_plan_list
        @phone_plan_list = Plan.select(:id, :name, :plan_minutes)
      end

      def permitted_params
        params.permit(:origin, :destination, :plan_id, :time)
      end

      def plan_params
        @plan_params = {
          minutes: Plan.find(permitted_params[:plan_id].to_i)[:plan_minutes],
          name: Plan.find(permitted_params[:plan_id].to_i)[:name]
        }
      end

      def make_calculation
        Fee.calculate(permitted_params, plan_params)
      end

    end
  end
end