class Fee < ApplicationRecord
  validates :origin, :destination, :price_minute, presence: true

  private

  def self.build_params
    @new_params = {
      origin: @origin,
      destination: @destination,
      time: @call_time,
      phone_plan: @phone_plan,
      with_plan: '$' + "%.2f" % @with_plan.round(2),
      without_plan: '$' + "%.2f" % @without_plan.round(2)
    }
  end

  def self.calculate(calculation_params, plan_params)
    
    # TODO: need to handle exception ActiveRecord::RecordInval and adapt build_params
    price_fee = Fee.find_by(origin: calculation_params[:origin].to_i,
                            destination: calculation_params[:destination].to_i)[:price_minute]
    available = plan_params[:minutes]
    @call_time = calculation_params[:time].to_i
    spent = available - @call_time
    @phone_plan = plan_params[:name]
    @origin = calculation_params[:origin]
    @destination = calculation_params[:destination]
    @with_plan  = spent >= 0 ? 0 : spent.abs * price_fee * 1.1
    @without_plan = @call_time * price_fee

    build_params
  end

end
