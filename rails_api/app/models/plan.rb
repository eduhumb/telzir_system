class Plan < ApplicationRecord
  validates :name, :plan_minutes, presence: true
end
