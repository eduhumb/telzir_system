class CreateFees < ActiveRecord::Migration[6.0]
  def change
    create_table :fees do |t|
      t.integer :origin
      t.integer :destination
      t.float :price_minute

      t.timestamps
    end
  end
end
