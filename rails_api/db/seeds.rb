# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# seed the Tarifa table with previous defined codes and prices
Fee.create(origin: 11, destination: 16, price_minute: 1.90)
Fee.create(origin: 16, destination: 11, price_minute: 2.90)
Fee.create(origin: 11, destination: 17, price_minute: 1.70)
Fee.create(origin: 17, destination: 11, price_minute: 2.70)
Fee.create(origin: 11, destination: 18, price_minute: 0.90)
Fee.create(origin: 18, destination: 11, price_minute: 1.90)

Plan.create(name: 'FaleMais 30', plan_minutes: 30)
Plan.create(name: 'FaleMais 60', plan_minutes: 60)
Plan.create(name: 'FaleMais 120', plan_minutes: 120)