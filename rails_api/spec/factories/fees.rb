FactoryBot.define do
  factory :fee do
    origin { Faker::Number.between(from: 11, to: 63) }
    destination { Faker::Number.between(from: 11, to: 63) }
    price_minute { Faker::Number.decimal(l_digits: 1, r_digits: 2) }
  end
end
