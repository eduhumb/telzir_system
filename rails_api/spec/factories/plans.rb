FactoryBot.define do
  factory :plan do
    name { Faker::Lorem.word }
    plan_minutes { Faker::Number.between(from: 30, to: 120) }
  end
end
