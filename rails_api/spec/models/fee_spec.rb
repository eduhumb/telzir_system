require 'rails_helper'

RSpec.describe Fee, type: :model do
  context "when creating new fee" do
    it { should validate_presence_of(:origin)}
    it { should validate_presence_of(:destination)}
    it { should validate_presence_of(:price_minute)}
  end
end
