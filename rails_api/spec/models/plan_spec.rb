require 'rails_helper'

RSpec.describe Plan, type: :model do
  it { should validate_presence_of(:name)}
  it { should validate_presence_of(:plan_minutes)}
end
