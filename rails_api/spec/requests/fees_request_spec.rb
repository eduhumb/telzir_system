require 'rails_helper'

RSpec.describe "Fees", type: :request do
  describe 'GET /api/v1/fees' do
    let!(:fees) { create_list(:fee, 10)}
    let!(:plans) { create_list(:plan, 3)}

    before { get '/api/v1/fees'}

    it 'returns fees' do
      expect(json).not_to be_empty
      expect(json['codes'].size).to eq(10)
      expect(json['plans'].size).to eq(3)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /api/v1/fees/calculate' do
    let(:valid_attributes) { { origin: 11, destination: 17, time: 80, plan_id: 2 } }

    context 'when the request is valid' do
      before { get '/api/v1/fees/calculate', params: valid_attributes}

      it 'returns the call fee', skip: true do
        expect(json).not_to be_empty
      end

      it 'returns status code ', skip: true do
        expect(response).to have_http_status(200)
      end
    end
  end
end
