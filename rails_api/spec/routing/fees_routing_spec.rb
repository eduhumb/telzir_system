require "rails_helper"

RSpec.describe "routes for Fees", :type => :routing do
  it "routes /fees to the fees controller" do
    expect(get("api/v1/fees")).
        to route_to(:controller => "api/v1/fees", :action => "index")
  end

  it "routes /fees/calculate to the fees controller" do
    expect(get("api/v1/fees/calculate")).
        to route_to(:controller => "api/v1/fees", :action => "show", :id => "calculate")
  end
end