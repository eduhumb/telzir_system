# Web front-end for Telzir Calculator

### TODO list
  
  - Form validation
  - Dinamic select from destination code by a selected origin
  - Select unique origins an destinations before displaying them in dropdown list

### Installation and usage

1. Install yarn (npm will break the lint)

  - https://yarnpkg.com/en/docs/install

2. Install the project dependencies

  - `yarn`

3. **Run server developed in the rails_api** (Otherwise the FrontEnd will not be able to retrieve data)

  - `See README from rails_api for instructions on how to run`

4. Run the front-end to consume the server API

  - `yarn start` - will run by default on **http://localhost:3000**

### Lint

The project lint uses the configuration provided by eslint package. To run the linter, just type:

  - `yarn lint`
