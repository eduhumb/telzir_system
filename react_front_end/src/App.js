import React from 'react';
import './App.css';

import Routes from './routes';

function App() {
  return (
    <div className="container">
      <div className="header">
        <img src={ require('./assets/logo_telzir.svg') }
          alt="company logo" height="50px" width="152px"
        />
      </div>
      <div className="content">
        <Routes />
      </div>
    </div>
  );
}

export default App;