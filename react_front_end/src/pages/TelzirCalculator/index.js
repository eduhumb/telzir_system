import React, { useEffect, useState } from 'react';
import api from '../../services/api';
import './styles.css';

export default function TelzirCalculator() {
  const search = window.location.search;
  const params = new URLSearchParams(search);
  const [codes, setCodes] = useState([]);
  const [plans, setPlans] = useState([]);
  const [result, setResult] = useState([]);
  const [origin, setOrigin] = useState(params.get('origin') ? params.get('origin') : '');
  const [destination, setDestination] = useState(params.get('destination') ? params.get('destination') : '');
  const [time, setTime] = useState(params.get('time') ? params.get('time') : '');
  const [planId, setPlanId] = useState(params.get('planId') ? params.get('planId') : '');
 
  useEffect(() => {
    calculate('calculate');
    setLists();
  }, []);

  async function setLists() {
      const response = await api.get(`/`);
      setCodes(response.data.codes);
      setPlans(response.data.plans);
  }

  async function calculate(type) {
    let query = '';
    switch (type) {
      case 'calculate':
        query = '?';
        if (origin && destination && time && planId) {
            query += `origin=${origin}&destination=${destination}&time=${time}&plan_id=${planId}&`;
        }
        break;
      default:
        clearInputs();
        break;
    }
    const response = await api.get(`/calculate${query}`);
    setResult(response.data);
  }

  function clearInputs() {
    setOrigin('');
    setDestination('');
    setPlanId('');
    setTime('');
  }

  function handleSubmit(event) {
    event.preventDefault();
    calculate('calculate');
  }

  return (
    <div className="calculator">
      <h1>Telzir Calculator</h1>
      <div className="calculator__options">
        <div className="calculator__filters">
          <div>
            <form onSubmit={ handleSubmit }>
              <select
                value={origin}
                onChange={(event) => setOrigin(event.target.value)}>
                {codes.map(code => (
                <option value={code.origin} key={code.id}>{code.origin}</option>
                ))}
              </select>
              <select
                value={destination}
                onChange={(event) => setDestination(event.target.value)}>
                {codes.map(code => (
                <option value={code.destination} key={code.id}>{code.destination}</option>
                ))}
              </select>
              <select
                value={planId}
                onChange={(event) => setPlanId(event.target.value)}>
                {plans.map(plan => (
                <option value={plan.id} key={plan.id}>{plan.name}</option>
                ))}
              </select>
              <input
                id="time"
                type="number"
                placeholder="Tempo (minutos)"
                value={ time }
                onChange={ (event) => setTime(event.target.value) }
              />
              <button type="submit" className="search"> Calcular </button>
            </form>
          </div>
        </div>
      </div>
      <div className="calculator_data__table-container">
        <table className="calculator_data__table">
            <tbody>
              <tr>
                <th><span>Origem</span></th>
                <th><span>Destino</span></th>
                <th><span>Tempo</span></th>
                <th><span>Plano FaleMais</span></th>
                <th><span>Com FaleMais</span></th>
                <th><span>Sem FaleMais</span></th>
              </tr>
              <tr>
                <td><span>{ result.origin }</span></td>
                <td><span>{ result.destination }</span></td>
                <td><span>{ result.time }</span></td>
                <td><span>{ result.phone_plan }</span></td>
                <td><span>{ result.with_plan }</span></td>
                <td><span>{ result.without_plan }</span></td>
              </tr>
            </tbody>
          </table>
      </div>
    </div>
  );
}