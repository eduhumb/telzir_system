import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import TelzirCalculator from './pages/TelzirCalculator';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={ TelzirCalculator } />
      </Switch>
    </BrowserRouter>
  );
}